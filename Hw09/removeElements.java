//julio Gonzalez
//delete index and number from array
//random assignment, delete, remove


//carr created main method
import java.util.Scanner;
import java.util.Arrays;

public class removeElements{
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
  //give number 1-9 to the array randomly
  public static int[] randomInput(){
    int[] random = new int[10];
    //go through array and assign 1-9
    for(int x = 0;x<10;x++){
      int y =(int) (Math.random()*(10));
      random[x] = y;
    }
    return random;
    
  }
  //delete a index on the array
  public static int[] delete(int[] list, int pos){
    int a = list.length;
    int b = a -1;
    Scanner myScanner = new Scanner(System.in);
    //check to see if number is within index
    while(pos>b){
      System.out.println(" enter a value within the index: ");
      pos = myScanner.nextInt();
    }
    
    
    int[] newArray = new int[b];
    
     //move every number after the postion back by 1 index 
    for(int x = pos;x<b;x++){
      list[pos] = list[pos+1];
      
      pos++;
    }
    //copy the array to a new array with one less index
    for(int x = 0;x<b;x++){
      newArray[x] = list[x];
      
    }
    return newArray;
  }
  //remove all numbers from array
  public static int[] remove(int[] list, int target){
    int b = list.length;
    
    int counter = 0;
    //loop through arrray
    for(int x = 0;x<b;x++){
      //check to see if array matches target
      if(list[x] == target){
        //keep count of numbers taken out
        counter++;
        //move list one place back from target number
        for(int y = x; y < list.length-2;y++){
          list[y] = list[y + 1];         
        }
        x--;
      }
      
    } 
    //new array with less index
    int[] newArray = new int[b-counter-1];
    for(int x = 0;x<b-counter-1;x++){
      newArray[x] = list[x];
      //copy array
    }
    return newArray;
  }
  
}