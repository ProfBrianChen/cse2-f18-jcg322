//julio gonzalez
//cse 02
// arrays - binary and linear search

import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;
public class CSE2Linear{
  //main method. what to do
  public static void main(String args[]){
    int[] input = new int[15];
    check(input);
    searchBinary(input);
    scramble(input);
    searchLinear(input);
  }
    
    
    
    
    //check to see the recuirements are met
  public static void check(int[] input){
    System.out.println("Enter 15 ascending ints for final grades in CSE2: ");
    
    Scanner myScanner = new Scanner(System.in);
    int x = 0;
    int y = 0;
    boolean correct = false;
    //check all numbers
    for(x = 0;x<15;x++){
      correct = false;
      correct = myScanner.hasNextInt();
      //check if it is integer
      if(correct){
        input[y] = myScanner.nextInt();
        //check 1-100
        if(input[y] < 101 && input[y] > 0){
          //check it is in ascending order
          if(y > 0 && input[y] < input[y-1]){
            
            x--;
            System.out.println("enter an integer bigger than the last one");
            //error message
          }
          else{
            y++;
            
            
           
          }
        }
        
        else{
          System.out.println("enter an integer between 1 and 100");
          //error message
          x--; //redo that loop for correct input
      }
        
      }
      else{  //error message
        System.out.println("enter an integer");
        myScanner.next();
        x--; //redo that loop for correct input

      }
    }
    //print array
    System.out.println(Arrays.toString(input));
  }
  
  //binary search
  public static void searchBinary(int[] input){
    int mid;
    int low;
    int high;
      
    low = 0;
    high = input.length - 1;
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Enter a grade to search for:");
    int key = myScanner.nextInt();
    int found = 0;
    int counter = 0;
    //loop until all numbers are checked
    while (high >= low) {
      
      counter ++;
         mid = (high + low) / 2;
         if (input[mid] < key) {
            low = mid + 1;
         } 
         if (input[mid] > key) {
            high = mid - 1;
         } 
         if(input[mid] == key){
            System.out.println(key +" was found in the list with " + counter + " iterations");
           found++;
           high = mid - 1;
         }
    }
         if(found == 0){
           System.out.println(key +" was not found in the list with " + counter + " iterations");
         }
    
  }
  //scramble array
  public static void scramble(int[] input){
    int x = input.length;
    int[] copy = new int[x];
    int[] mix = new int[x];
    int i;
    int index = 0;
    //loop through array
    for(i = 0; i < x; i++){
      if(copy[i] ==0  ){
        Random rand = new Random();
        index = rand.nextInt(5);
        index = (int) (Math.random()*(x));
        //check to see if the number has been used
        if(input[index] != 0){
          copy[i] = input[index];
          input[index] = 0;
          
         
          //System.out.println(index);
        }
        
        else{
          i--;
        }
      }
      else{
        i--;
      }
    }
    //copy new array to old array
      for(i = 0; i < x; i++){
        input[i] = copy[i];
      }
    System.out.println("Scrambled: ");
    
      System.out.println(Arrays.toString(input));
    }
  
  //linear search
  public static void searchLinear(int[] input){
    int x = input.length;
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Enter a grade to search for:");
    int key = myScanner.nextInt();
    int counter = 0;
    int i = 0;
    for(i = 0; i < x; i++){
      if(input[i] == key){
        int y = i + 1;
        System.out.println(key +" was found in the list with " + y + " iterations");
        counter = 1;
      }
      
    }
    if(counter == 0){ //check to see if number was found
      
      System.out.println(key +" was not found in the list with " + x +" iterations");
    }
    
    
  }
    
    
    
    
    
    
  }
  