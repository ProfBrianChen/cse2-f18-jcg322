import java.util.Scanner;
import java.util.Arrays;

public class testtest{
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
  public static int[] randomInput(){
    int[] random = new int[10];
    for(int x = 0;x<10;x++){
      int y =(int) (Math.random()*(10));
      random[x] = y;
    }
    return random;
    
  }
  public static int[] delete(int[] list, int pos){
    int a = list.length;
    int b = a -1;
    Scanner myScanner = new Scanner(System.in);
    while(pos>b){
      System.out.println(" enter a value within the index: ");
      pos = myScanner.nextInt();
    }
    
    
    int[] newArray = new int[b];
    
      
    for(int x = pos;x<b;x++){
      list[pos] = list[pos+1];
      
      pos++;
    }
    for(int x = 0;x<b;x++){
      newArray[x] = list[x];
      
    }
    return newArray;
  }
  public static int[] remove(int[] list, int target){
    int b = list.length;
    
    int counter = 0;
    for(int x = 0;x<b;x++){
      if(list[x] == target){
        counter++;
        for(int y = x; y < list.length-2;y++){
          list[y] = list[y + 1];         
        }
        x--;
      }
      
    } 
    int[] newArray = new int[b-counter-1];
    for(int x = 0;x<b-counter-1;x++){
      newArray[x] = list[x];
      
    }
    return newArray;
  }
  
}
