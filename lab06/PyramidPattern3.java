import java.util.Scanner;
// CSE 002 lab 06
//get a pattern using numbers
//Julio Gonzalez
public class PyramidPattern3{
   // main method required for every Java program
   public static void main(String[] args) {
     //define and initiate
     int row=0;
    
    
    //loop, keeps asking until user inputs 1-10
    Scanner myScanner = new Scanner(System.in);
    System.out.println("how many rows would you like? ");
    
    while(row< 1 || row > 10){
      System.out.println("enter an integer 1-10");
      row = myScanner.nextInt();
     

      }
     
     
     
     
     
    //nested loop for pattern
    for (int kk = 1; kk <=row; kk++) {
      
			for (int mm = row; mm >= kk; mm--) {
        System.out.print(" ");
      }
        for(int aa = kk; aa>=1;aa--){
          System.out.print(aa);
        }
        
				
        
			
      
			System.out.println();

      
			
		}


    
    
    
    
    
  }
}