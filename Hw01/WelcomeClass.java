
  ///////
//// CSE 02 WelcomeClass
///
public class WelcomeClass{
  
  public static void main(String args[]){
    //prints wlecome class to terminal window
    System.out.println("   -----------");
    System.out.println("   l WELCOME l");
    System.out.println("   -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-E--J--K--O--0--0->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  V  V  V  V  V  V");
  }
}