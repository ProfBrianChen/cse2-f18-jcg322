//Julio Gonzalez
//CSE 002 - word Tools
//create a menu with different functions using a user input



//import scanner
import java.util.Scanner;


public class wordTools{
  public static void main(String args[]){
    //define variable
    String text = "null";
    String choose = "null";
    String edit = "null";
    Scanner myScanner = new Scanner(System.in);
    
    text = sampleText(myScanner);
    choose = printMenu(myScanner);
    //continues until the user inputs "q"
    while(!choose.equals("q")){
      
   //pass methods based on user input
      if(choose.equals("c")){
        getNumOfNonWSCharacters(text); 
      }
      else if(choose.equals("w")){
        getNumOfWords(text); 
        
      }
      else if(choose.equals("f")){
        findText(text); 
      }
      else if(choose.equals("r")){
        replaceExclamation(text); 
      }
      else if(choose.equals("s")){
        System.out.println("Edited text: " + shortenSpace(text)); 
      }
      choose = printMenu(myScanner);
 
      
    }
   
  }
  
  
  //method to get user input
  public static String sampleText(Scanner myScanner){
    String userInput = "null";
    System.out.println("Enter a sample text: ");
    userInput = myScanner.nextLine();
    System.out.println("You entered: ");
    System.out.println(userInput);
    return userInput;

  }
  
  
  
  //method to print the menu screen with options
  public static String printMenu(Scanner myScanner){
    String choose = "null";
    boolean statement = true;
    System.out.println("MENU");
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    
    System.out.println("choose an option: ");
    //loop to repeat until th user enter correct input
    while(statement == true){ 
      choose = myScanner.nextLine();
      if(choose.equals("c") || choose.equals("w") || choose.equals("f") || choose.equals("r") || choose.equals("s") || choose.equals("q") ){
        statement = false;
      }
         else{
           System.out.println("invalid input. try again");
         }
    }
    //return the users choice
    return choose;
  }
  
  
  //method to get non-white spaces
  public static void getNumOfNonWSCharacters(String text) {
    char a;
    int i = 0;
    int counter = 0;
    int b;
    a = text.charAt(i);
    b = text.length();
    //loop to find spaces until i is the length of the text
    while(i < (b-1)){
      if(a == ' '){
        counter += 1;
      }
      //System.out.println(i);
      i +=1;
      a = text.charAt(i);
      
    }
    //take length of text and sutract the spaces
     System.out.println("Number of non-whitespace characters: " + (b-counter));
  }
  
  //method to get the number of spaces
  public static void getNumOfWords(String text) {
    //cal shortenspace() method to remove extra spaces
    text = shortenSpace(text);
    char a;
    int i = 0;
    int counter = 1;
    int b;
    a = text.charAt(i);
    b = text.length();
    //count the spaces since each word has a space after
    while(i < (b-1)){
      if(a == ' '){
        counter += 1;
      }
      //System.out.println(i);
      i +=1;
      a = text.charAt(i);
      
    }
    //print number of words
     System.out.println("Number of words: " + counter);
  }

    
  
  
  public static void findText(String text) {
    String wordToFind = "null";
    //use scanner to get user input on word to find text
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Enter a word or phrase to be found:");
    wordToFind = myScanner.next();
    //replace all 0's with 1's
    text = text.replaceAll("0", "1");
    //replace all matching words from user input with 0's
    text = text.replaceAll(wordToFind, "0");
    char a;
    int i = 0;
    int counter = 0;
    int b;
    a = text.charAt(i);
    b = text.length();
    //count the times 0 appears
    while(i < (b-1)){
      if(a == '0'){
        counter += 1;
      }
      //System.out.println(i);
      i +=1;
      a = text.charAt(i);
      
    }
     System.out.println("\"" + wordToFind + "\" instances : " + counter);
  }
  public static void replaceExclamation(String text) {
//replace the exclamation with a period
    text = text.replaceAll("!", ".");
//print edited text
    System.out.println("Edited text: " + text);

  }
//method to get rid of extra spaces  
  public static String shortenSpace(String text) {
    
   text = text.replaceAll("\\s+", " ");
    
      
    

    
    return text;
    
  }






}