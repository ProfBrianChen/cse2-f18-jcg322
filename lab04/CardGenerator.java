

public class CardGenerator{
  public static void main(String[] args){
    int cardNumber;
    String cardNum;
    String cardSuit="null";
    String cardString;
   
    
    cardNumber = (int) (Math.random()*(52))+1;   //get a random number
    System.out.println("The number was " + cardNumber);
    
    
    //choose the card suite
    if(cardNumber>=1 && cardNumber <= 13){
      cardSuit = "diamonds";
    }
    else if (cardNumber>=14 && cardNumber <= 26){
      cardSuit = "clubs";
    }
    else if(cardNumber>=27 && cardNumber <= 39){
      cardSuit = "hearts";
    }
    else if(cardNumber>=40 && cardNumber <= 52){
      cardSuit = "spades";
    }
    
   
    cardNumber = cardNumber % 4; //change card number into 0-12
    //switch statements for 0-12
    switch (cardNumber){
      case 0: cardString = "King";
        break;
      case 1: cardString = "Ace";
        break;
      case 2: cardString = "2";
        break;
      case 3: cardString = "3";
        break;
      case 4: cardString = "4";
        break;
      case 5: cardString = "5";
        break;
      case 6: cardString = "6";
        break;
      case 7: cardString = "7";
        break;
      case 8: cardString = "8";
        break;
      case 9: cardString = "9";
        break;
      case 10: cardString = "10";
        break;
      case 11: cardString = "Jack";
        break;
      case 12: cardString = "Queen";
        break;
      default: cardString = "invalid card";
    break; 
      
        
    }
    //print results
    System.out.println("You picked the " + cardString + " of " + cardSuit);
    
    
    
  }
}