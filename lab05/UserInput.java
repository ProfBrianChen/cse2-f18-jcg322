import java.util.Scanner;

// CSE 002 lab 05
//use loops to get right response from user
//Julio Gonzalez
public class UserInput{
   // main method required for every Java program
  public static void main(String[] args) {
    boolean correct = false;
    boolean meetWeek = false;
    boolean timeStart = false;
    boolean teacherName = false;
    boolean deptName = false;
    boolean numStudents = false;
    int x= 0;
    int y = 0;
    int z = 0;
    int b = 0;
    String a = "";
    String c = "";
    
    Scanner myScanner = new Scanner(System.in);
    
    //course number//
    
    while(correct == false){
      System.out.println("course number?");
      correct = myScanner.hasNextInt();
      if(correct){
        x = myScanner.nextInt();

      }
      else{
        System.out.println("enter an integer");
        myScanner.next();
      }
    }

    System.out.println(x);
    
    
    while(deptName == false){
      System.out.println("What is your department's name? (last name only)");
      deptName = myScanner.hasNext();
      if(deptName){
        c = myScanner.next();

      }
      else{
        System.out.println("enter your department name");
        myScanner.next();
      }
    }

    System.out.println(c);
    
    
    
    
    
    
    while(meetWeek == false){
      System.out.println("how many times does it meets in a week?");
      meetWeek = myScanner.hasNextInt();
      if(meetWeek){
        y = myScanner.nextInt();

      }
      else{
        System.out.println("enter an integer");
        myScanner.next();
      }
    }

    System.out.println(y);
    
    
    
    
    while(timeStart == false){
      System.out.println("What time does your class start at in military time? (ex. if it is 9:25, write 925)");
      timeStart = myScanner.hasNextInt();
      if(timeStart){
        z = myScanner.nextInt();

      }
      else{
        System.out.println("enter an integer");
        myScanner.next();
      }
    }

    System.out.println(z);
    
    
     while(teacherName == false){
      System.out.println("What is your professor's name? (last name only)");
      teacherName = myScanner.hasNext();
      if(teacherName){
        a = myScanner.next();

      }
      else{
        System.out.println("enter your professors name");
        myScanner.next();
      }
    }

    System.out.println(a);
    
    
     while(numStudents == false){
      System.out.println("How many students are in your class?");
      numStudents = myScanner.hasNextInt();
      if(numStudents){
        b = myScanner.nextInt();

      }
      else{
        System.out.println("enter an integer");
        myScanner.next();
      }
    }

    System.out.println(b);
   
    System.out.println( "you are taking course number " + x + ". Your department name is " + c +
                      ". The class meets " + y + " a week at " + z + " in " + 
                      a + "'s class that has " + b + " students");
    
  } 
  
}