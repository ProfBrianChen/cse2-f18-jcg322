//Julio Gonzalez
//CSE 002
//CrapsIf- game where you roll the 2 dice and that gives you a "slang term", 
//the program will get two numbers randomly or user input using only if and else if statements





import java.util.Scanner;

public class CrapsIf{
  public static void main(String args[]){
    int diceOne = 0;
    int diceTwo = 0;
    String slangName = "null";
  
  
  
  
  //ask if they would like random or user assigned numbers 
    Scanner myScanner = new Scanner(System.in);
    System.out.println("would you like to roll randomly or the state two dices: 1 = State numbers or 2 = roll ramdonly ");
    int diceAnswer = myScanner.nextInt();
    if(diceAnswer == 1){
      System.out.println("what is the value of the first dice");
      diceOne = myScanner.nextInt();
      System.out.println("what is the value of the second dice");
      diceTwo = myScanner.nextInt();
    }
    else if(diceAnswer == 2){
      diceOne = (int) (Math.random()*(6))+1;
      diceTwo = (int) (Math.random()*(6))+1;
    
    }
   // if statements for slang term  
    if(diceOne ==1 && diceTwo == 1){
      slangName = "Snake Eyes";
    }
    else if(diceOne == 1 && diceTwo == 2 || diceTwo == 1 && diceOne ==2){
      slangName = "Ace Deuce";
    }
    else if(diceOne == 2 && diceTwo == 2){
      slangName = "Hard Four";
    }
    else if(diceOne == 3 && diceTwo == 3){
      slangName = "Hard Three";
    }
    else if(diceOne == 4 && diceTwo == 4){
      slangName = "Hard Eight";
    }
    else if(diceOne == 5 && diceTwo == 5){
      slangName = "Hard Ten";
    }
    else if(diceOne == 6 && diceTwo == 6){
      slangName = "Boxcars";
    }
    else if(diceOne == 1 && diceTwo == 3 || diceTwo == 1 && diceOne ==3){
      slangName = "Easy Four";
    }
    else if(diceOne == 2 && diceTwo == 3 || diceTwo == 2 && diceOne ==3){
      slangName = "Fever Five";
    }
    else if(diceOne == 1 && diceTwo == 4 || diceTwo == 1 && diceOne ==4){
      slangName = "Fever Five";
    }
    else if(diceOne == 2 && diceTwo == 4 || diceTwo == 2 && diceOne ==4){
      slangName = "Easy Six";
    }
    else if(diceOne == 1 && diceTwo == 5 || diceTwo == 1 && diceOne ==5){
      slangName = "Easy Six";
    }
    else if(diceOne == 1 && diceTwo == 6 || diceTwo == 1 && diceOne ==6){
      slangName = "Seven Out";
    }
    else if(diceOne == 2 && diceTwo == 5 || diceTwo == 2 && diceOne ==5){
      slangName = "Seven Out";
    }
    else if(diceOne == 4 && diceTwo == 3 || diceTwo == 4 && diceOne ==3){
      slangName = "Seven Out";
    }
    else if(diceOne == 5 && diceTwo == 3 || diceTwo == 5 && diceOne ==3){
      slangName = "Easy Eight";
    }
    else if(diceOne == 2 && diceTwo == 6 || diceTwo == 2 && diceOne ==6){
      slangName = "Easy Eight";
    }
    else if(diceOne == 6 && diceTwo == 3 || diceTwo == 6 && diceOne ==3){
      slangName = "Nine";
    }
    else if(diceOne == 4 && diceTwo == 5 || diceTwo == 4 && diceOne ==5){
      slangName = "Nine";
    }
    else if(diceOne == 6 && diceTwo == 4 || diceTwo == 6 && diceOne ==4){
      slangName = "Easy Ten";
    }
    else if(diceOne == 5 && diceTwo == 6 || diceTwo == 5 && diceOne ==6){
      slangName = "Yo-leven";  
    }
    //print number and slang term 
    System.out.println("Your dice was " + diceOne + " and " + diceTwo + " ,and you rolled a " + slangName);
    
  
  
  
  
  }
}