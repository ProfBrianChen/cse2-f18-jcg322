//Julio Gonzalez
//CSE 002
//Poker- consider the chances of gettting different hands









import java.util.Scanner;

public class poker {
    public static void main(String[] args) {

        int numTwoPairs = 0; //number of two pair hands
        int numOnePair = 0; //number of one pair hands
        int numFourKind = 0; //number of two pair hands
        int numThreeKind = 0; //number of one pair hands
        
        Scanner myScanner = new Scanner(System.in); 

        //check to see if input is an integer
        System.out.println("How many hands would you like? ");
        while (!myScanner.hasNextInt()) {
            myScanner.next();
            System.out.println("integers only!");
}
        int totalhands = myScanner.nextInt();

        //loop for total number of hands 
        int hand = 1; 
        while (hand <= totalhands)
        {
            boolean twopair = false;
            boolean onepair = false;

            boolean fourKind = false;
            boolean threeKind = false;
 
            //hand draws 5 different cards
            int card1 = (int)(Math.random()*52)+1;
            int card2 = (int)(Math.random()*52)+1;
            while (card2 == card1){
            
                card2 = (int)(Math.random()*52)+1;
            }
            int card3 = (int)(Math.random()*52)+1;
            while ((card3 == card1) || (card3 == card2)){
            
                card3 = (int)(Math.random()*52)+1;
            }
            int card4 = (int)(Math.random()*52)+1;
            while ((card4 == card1) || (card4 == card2)||(card4 == card3)){
            
                card4 = (int)(Math.random()*52)+1;
            }
            int card5 = (int)(Math.random()*52)+1;
            while ((card5 == card1) || (card5 == card2)||(card5 == card3)||(card5 == card4)){
            
                card5 = (int)(Math.random()*52)+1;
            }
 
     
            int card1suit = (card1-1)/13;
            int card2suit = (card2-1)/13;
            int card3suit = (card3-1)/13;
            int card4suit = (card4-1)/13;
            int card5suit = (card5-1)/13;

            int card1num = (card1%13);
            int card2num = (card2%13);
            int card3num = (card3%13);
            int card4num = (card4%13);
            int card5num = (card5%13);
          
            if (
((card1num == card2num && card1num == card3num && card1num == card4num))||
((card2num == card1num && card2num == card3num && card2num == card4num))||
((card3num == card1num && card3num == card2num && card3num == card4num ))||
((card4num == card1num && card4num == card2num && card4num == card3num))||
((card5num == card1num && card5num == card2num && card5num == card3num && card5num == card4num)))
            {
                numFourKind++;
                fourKind = true;
                
            }

           
            if (fourKind == false){
                if ((card1num == card2num && card1num == card3num)||
(card1num == card3num && card1num == card4num)||(card1num == card4num && card1num == card5num)||
(card2num == card1num && card2num == card3num)||
(card3num == card1num && card3num == card2num)||(card3num == card4num && card3num == card5num)||
(card4num == card1num && card4num == card2num)||(card4num == card3num && card1num == card5num)||
(card5num == card1num && card5num == card2num)||(card5num == card3num && card1num == card4num))
                {
                    numThreeKind++;
                    threeKind = true;
                   
                }
            }
            
            if (((card1num == card2num)&&(card3num == card4num))||
((card1num == card3num)&&(card2num == card4num))||((card1num == card4num)&&(card2num == card3num))||((card1num == card2num)&&(card3num == card5num))||
((card1num == card3num)&&(card2num == card5num))||((card1num == card4num)&&(card3num == card5num))||
((card1num == card3num)&&(card2num == card5num))||
((card3num == card2num)&&(card5num == card4num))||
((card1num == card3num)&&(card5num == card4num)))
            {
                numTwoPairs++;
                twopair = true;
                
            }

           
            if (twopair == false)
            {
                if ((card1num == card2num)||
(card1num == card3num)||(card1num == card4num)||(card1num == card5num)||
(card2num == card3num)||(card2num == card4num)||(card2num == card5num)||
(card3num == card4num)||(card3num == card5num)||
(card4num == card5num))
                {
                    numOnePair++;
                    onepair = true;
                    
                }
            }
            hand++;
        }
 
        //print chances of hands
        System.out.println("Total loops: " + totalhands);
        System.out.printf("The probability of getting a 4 of a kind: %1.3f  \n", ((double)numFourKind / totalhands));
        System.out.printf("The probability of getting a 3 of a kind: %1.3f \n", ((double)numThreeKind / totalhands));
        System.out.printf("The probability of getting a Two pair: %1.3f \n", ((double)numTwoPairs / totalhands));
        System.out.printf("The probability of getting a One pair: %1.3f \n", ((double)numOnePair / totalhands));
    }

}