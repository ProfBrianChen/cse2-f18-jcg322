  ///////
//// CSE 02 Arithmetic
///
public class Arithmetic{
  
  public static void main(String args[]){
    //Declarations
    
    double totalCostOfPants;    //Total Cost Of Pants
    double totalCostOfShirts;   //Total Cost Of sweatshirts
    double totalCostOfBelts;    //Total Cost Of belts
    double taxOfPants;          //Tax of Pants
    double taxOfShirt;          //Tax of sweatshirts
    double taxOfBelts;          //Tax of belts
    double costBeforeTax;       //Total cost of items before tax
    double totalTax;            //Total taxes
    double totalPurchasePrice;
    
    
    //assumtions given
    int numPants = 3;                //Number of pairs of pants
    double pantsPrice = 34.98;       //Cost per pair of pants
    int numShirts = 2;               //Number of sweatshirts
    double shirtPrice = 24.99;       //Cost per shirt
    int numBelts = 1;                //Number of belts
    double beltCost = 33.99;         //cost per belt
    double paSalesTax = 0.06;        //the tax rate
    
    
    //total cost
    totalCostOfPants = numPants * pantsPrice;      //total cost of all the pants
    totalCostOfShirts = numShirts * shirtPrice;    //total cost of all the sweatshirts
    totalCostOfBelts = numBelts * beltCost;        //total cost of all the belts
    costBeforeTax = totalCostOfShirts + totalCostOfPants + totalCostOfBelts;      //total cost of perchase before tax
    
    //Sales tax 
    taxOfPants = totalCostOfPants * paSalesTax;//Tax of Pants
    taxOfPants = (int) (taxOfPants * 100) / 100.0;
    taxOfShirt = totalCostOfShirts * paSalesTax;   //Tax of sweatshirts
    taxOfBelts = totalCostOfBelts * paSalesTax;          //Tax of belts
    totalTax = taxOfPants + taxOfShirt + taxOfBelts;
    
    //Total purchase price
    totalPurchasePrice = costBeforeTax + totalTax;
    
    //print total cost of each item
    System.out.println("The cost of pants is $" + totalCostOfPants);
    System.out.println("The cost of sweatshirts is $" + totalCostOfShirts);
    System.out.println("The cost of belts is $" + totalCostOfBelts);
    
    //print tax of each item
    System.out.println("The tax of pants is $" + taxOfPants);
    System.out.println("The tax of sweatshirts is $" + taxOfShirt);
    System.out.println("The tax of belts is $" + taxOfBelts);
    
    //print total purchase perchase price before tax
    System.out.println("The total perchase cost before taxes is $" + totalPurchasePrice);
    //print the total taxes
    System.out.println("The total taxes for the purchase is $" + totalTax);
    //print total purchase price
    System.out.println("The total cost of the purchase is $" + totalPurchasePrice);
    

  } 
}