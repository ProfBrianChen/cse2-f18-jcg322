//Julio Gonzalez
//CSE 02
//tic-tac-toe game 

import java.util.Arrays;
import java.util.Scanner;

public class Hw10{
  //main method
  public static void main(String[] args){
    int counter = 0;
    String[] player = new String[2];
    String[][] board = {
      {"1","2","3"},
      {"4","5","6"},
      {"7","8","9"},
      
    };
    //prints the board
    printBoard(board);
    //asks player if the wan to be x or o
    askPLayer(player);
     //System.out.println(Arrays.toString(player));
    int x = 0;
    //play for 9 turns
    while(x<9){
        System.out.println("x = " + x);
      if(x%2 == 0){
        counter = play(board, player[0]);
        //plays turn on player 1
        
      }
      if(x%2 == 1){
        counter = play(board, player[1]);
        //plays turn on player 2 
      }
      if(counter == 1){
          counter = 0;
        x--;
        //if a spot is taken, turn returns back 1
      }
      int winner = checkWinner(board);
      if(winner>=0 && winner<=7){
        x = 9;
        //if there is a winner, loop stops
      }
      x++;
      if(x == 9){
        //if all 9 truns are used up and no winner is declared
            System.out.println("game ended in a draw.");
      }
    }
    
    
  }  //prints the board
   public static void printBoard(String[][] board){
     for(int row = 0; row < 3; row++)//loop through array
   {
       System.out.print("[");
      for(int column = 0; column < 3; column++)
      {
         System.out.print(board[row][column] + " ");
      }
      System.out.println("]");
       
   }

   }  //asks player to between o and x
  public static void askPLayer(String[] player){
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Player 1: ");
    //String entry = "null";
    char y = 'n';
    char a = 'o';
    char b = 'x';
    
    //checks input to see if it is correct
    while(y != a || y != b ){
      System.out.println(" use \"x\" or \"o\" only ");
      y = myScanner.next().charAt(0);
      if(y == a || y == b){
        break;
      }
    }
    String player1 = Character.toString(y); //changes to string
    if(y == a){
      player[0]= player1;
      player[1] = Character.toString(b);
    }
    else{
      player[0]= player1;
      player[1] = Character.toString(a);
    }
           
    
  }
  public static int positionCheck(){
    System.out.println("player  enter position on board to place mark.");
    
    
    
    Scanner myScanner = new Scanner(System.in);
    int position = 0;
    boolean z = false;
    //checks to see if it is 1-9
    while(z == false){
      boolean y = myScanner.hasNextInt();
      while(y == false){
          System.out.println("enter an integer");
          myScanner.nextLine();
          y = myScanner.hasNextInt();
          
        
        
      }
      position = myScanner.nextInt();
      if (position > 0 && position < 10){
        z = true;
        
      } 
      else {
        System.out.println("player enter position on board to place mark.");
        y = myScanner.hasNextInt();
        while(y == false){
          System.out.println("enter an integer");
          myScanner.nextLine();
          y = myScanner.hasNextInt();
         
        
        
      }
         position = myScanner.nextInt();
        System.out.println(position);
      }
  }
    return position;
  }
  //places the mark on the board
  public static int play(String[][] board, String mark){
    int position = positionCheck();
    int counter = 0;
      switch(position){
          //if there is a number there, the the mark is placed, else counter = 1 is returned
      case 1 : 
          if(board[0][0].equals("1")){
            board[0][0] = mark;
          }
          else{
                System.out.println("space is taken");
            
            counter = 1;
          }
          break;
      case 2 : 
          if(board[0][1].equals("2")){
            board[0][1] = mark;
          }
          else{
                System.out.println("space is taken");
           
            counter = 1;
          }
          break;
      case 3 : 
          if(board[0][2].equals("3")){
            board[0][2] = mark;
          }
          else{
                System.out.println("space is taken");
            
            counter = 1;
          }
          break;
      case 4 : 
          if(board[1][0].equals("4")){
            board[1][0] = mark;
          }
          else{
                System.out.println("space is taken");
            
            counter = 1;
          }
          break;
      case 5 : 
           
          if(board[1][1].equals("5")){
            board[1][1] = mark;
          }
          else{
                System.out.println("space is taken");
            
            counter = 1;
          }
          break;
      case 6 : 
          
          if(board[1][2].equals("6")){
            board[1][2] = mark;
          }
          else{
                System.out.println("space is taken");
            
            counter = 1;
          }
          break;
      case 7 : 
          
          if(board[2][0].equals("7")){
            board[2][0] = mark;
          }
          else{
                System.out.println("space is taken");
            
            counter = 1;
          }
          break;
      case 8 : 
           
          if(board[2][1].equals("8")){
            board[2][1] = mark;
          }
          else{
                System.out.println("space is taken");
            play(board,mark);
           
          }
          break;
      case 9 : 
          
          if(board[2][2].equals("9")){
            board[2][2] = mark;
            
          }
          else{
                System.out.println("space is taken");
            play(board,mark);
            
          }
          break;
    
    }
    printBoard(board); //prints the board
      System.out.println(counter); 
      return counter;

  } //checks o and x input
  public static char check(){
  char y = 'n';
    char a = 'o';
    char b = 'x';
    
    Scanner myScanner = new Scanner(System.in);
    while(y != a || y != b ){
      System.out.println(" use \"x\" or \"o\" only ");
      y = myScanner.next().charAt(0);
      if(y == a || y == b){
        break;
      }
    }
    return y;
  } //checks for a winner
   public static int checkWinner(String [][] board){
     int counter = 20;
     String [] threeRowv1 = new String [3];
     String [] threeColumn1 = new String [3];
     for(int row = 0; row<3;row++){
       for(int column = 0; column < 3;column++){ //loops through array horizontally
       threeRowv1[column]=board[row][column];
         if(threeRowv1[0].equals(threeRowv1[1]) && threeRowv1[0].equals(threeRowv1[2])){ //checks row to see if it is equal
            counter = row;
         }
         
       }
       //resets check array 
       threeRowv1[1] = "0";
       threeRowv1[2] = "0";
       threeRowv1[0] = "0";
     }
     
     //check for equal vertically
     for(int b = 0; b<3;b++){
       
       for(int a = 0; a < 3;a++){
         threeColumn1[a]=board[a][b];
         if(threeColumn1[0].equals(threeColumn1[1]) && threeColumn1[0].equals(threeColumn1[2])){
            counter = 3 + b;
         }
        
       }
       //reset array
       threeColumn1[0] = "0";
       threeColumn1[1] = "0";
       threeColumn1[2] = "0";
     }
     //checks diagonal winner
     if(board[0][0].equals(board[1][1] ) && board[0][0].equals(board[2][2])){
       counter = 6;
     }
     if(board[0][2].equals(board[1][1] ) && board[0][2].equals(board[2][0])){
       counter = 7;
     }
     //prints out who wins if there is a match in 3 places
     switch(counter){
       case 0:  System.out.println("winner :"  + board[0][0]);
         break;
       case 1:  System.out.println("winner :"  + board[1][0]);
         break;
       case 2:  System.out.println("winner :"  + board[2][0]);
         break;
       case 3:  System.out.println("winner :"  + board[0][0]);
         break;
       case 4:  System.out.println("winner :"  + board[0][1]);
         break;
       case 5:  System.out.println("winner :"  + board[0][2]);
         break;
       case 6:  System.out.println("winner :"  + board[0][0]);
         break;
       case 7:  System.out.println("winner :"  + board[0][2]);
         break;
     }
      
     return counter;
     
     
     
     
   }
  








}

