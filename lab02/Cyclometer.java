
  //measures speed, distance and rotations of trips on a bicycle
//// CSE 02 Cyclometer
///
public class Cyclometer{
  
  public static void main(String args[]){
    //our input data
    int secsTrip1=480;  // time of first trip in seconds
    int secsTrip2=3220;  // time of second trip in seconds
		int countsTrip1=1561;  //rotation counts of first trip
		int countsTrip2=9037; //rotation count of second trip
    
    
    //our intermediate variables and output
    double wheelDiameter=27.0,  //diameter of wheel
  	PI=3.14159, // value of pi to determine circumference later
  	feetPerMile=5280,  // number of feet in a mile
  	inchesPerFoot=12,   // number of inches in a foot
  	secondsPerMinute=60;  // number of seconds in a minute
	  double distanceTrip1, distanceTrip2,totalDistance;  // defines the variable for distance of trip 1 and 2 and the total distance
    
    
    //print out the output data
    System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
    System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
    
    
    //run the calculations; store the values. Document your
		//calculation here. What are you calculating?
		//
		//
	distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	totalDistance=distanceTrip1+distanceTrip2;
    
    
    //Print out the output data.
  System.out.println("Trip 1 was " + distanceTrip1 + " miles");
	System.out.println("Trip 2 was " + distanceTrip2 + " miles");
	System.out.println("The total distance was " + totalDistance + " miles");

    
    
    
    
      } //end of main method
} //end of class