import java.util.Scanner;


public class Pyramid{
  public static void main(String[] args) {
    
    
    Scanner myScanner = new Scanner(System.in);
    System.out.print("The square side of the pyramid is (input length): ");
    double baseLength = myScanner.nextInt(); //input base length
    System.out.print("The height of the pyramid is (input height): ");
    double height = myScanner.nextInt(); //input height of pyramid
    
    
    double areaOfPyramid = baseLength * baseLength * height / 3.0; //formula for volume
    
    System.out.print("The volume inside the pyramid is: " + areaOfPyramid + ".");
    
  }
}