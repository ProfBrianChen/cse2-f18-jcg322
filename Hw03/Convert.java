import java.util.Scanner;
// CSE 002 HW 03
//split a check
//
public class Convert{
   // main method required for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);   //create scanner
    System.out.print("Enter the affected area in acres: ");
    double affectedArea = myScanner.nextDouble(); //make response a double; area affected
    System.out.print("Enter the rainfall in the affected area: ");
    double rainInches = myScanner.nextDouble(); //make response on rain in inches into a double
    
    
    
    double rainPerAcre = rainInches * affectedArea; //total rainfall in inches/acre
    double gallons = rainPerAcre * 27154.285990761; //convert to gallons
    double cubicMiles = gallons / 1101117147428.6; //convert to cubic miles
   
    
    System.out.print(cubicMiles + " cubic miles");
      
      
    
    
  }
}